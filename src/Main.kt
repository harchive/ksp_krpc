import utils.KspProgram
import utils.executors.Fixer
import utils.executors.ToOrbitExecutor
import utils.executors.ToSatelliteOrbitExecutor

object Main {

    @JvmStatic
    fun main(args: Array<String>) = KspProgram.run {

        ToOrbitExecutor.toOrbit(this, 100000, 0, 90000)
        /*val muna = spaceCenter.bodies["Mun"] ?: return@run
        ToSatelliteOrbitExecutor.execute(this, muna)

        vessel.control.activateNextStage()
        Fixer.fixY(this, 100)*/

    }

}