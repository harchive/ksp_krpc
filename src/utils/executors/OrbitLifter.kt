package utils.executors

import utils.KspProgram


object OrbitLifter {

    fun circulizeUp(program: KspProgram) = with(program) {
        val apoapsisHeight = vessel.orbit.apoapsis
        liftUp(this, apoapsisHeight)
    }

    fun getLiftUpDV(program: KspProgram, newHeight: Double) = with(program) {
        val mu = vessel.orbit.body.gravitationalParameter
        val r = vessel.orbit.apoapsis
        val a1 = vessel.orbit.semiMajorAxis
        val a2 = (r + newHeight) / 2
        val v1 = Math.sqrt(mu * ((2.0 / r) - (1.0 / a1)))
        val v2 = Math.sqrt(mu * ((2.0 / r) - (1.0 / a2)))
        return@with v2 - v1
    }

    fun liftUp(program: KspProgram, newHeight: Double) = with(program) {
        val dV = getLiftUpDV(this, newHeight).toFloat()
        vessel.control.addNode(spaceCenter.ut + vessel.orbit.timeToApoapsis, dV, 0f, 0f)
        NodeExecutor.execFirst(this)
    }

}