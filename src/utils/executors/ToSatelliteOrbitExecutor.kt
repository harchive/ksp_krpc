package utils.executors

import krpc.client.services.SpaceCenter
import utils.KspProgram
import utils.KspUtils
import utils.log.Log


object ToSatelliteOrbitExecutor {

    private val PERIOD_PARTS_COUNT = 1000

    fun execute(kspProgram: KspProgram, satellite: SpaceCenter.CelestialBody) = with(kspProgram) {
        if (vessel.orbit.nextOrbit != null) {
            Log.w("Vessel orbit is not simple")
            return@with
        }

        val vesselBody = vessel.orbit.body
        val vesselBodySatellites = vesselBody.satellites
        if (!vesselBodySatellites.contains(satellite)) {
            Log.w("${vesselBody.name} has no satellite ${satellite.name}")
            return@with
        }

        val period = vessel.orbit.period
        val dt = period / PERIOD_PARTS_COUNT
        val node = vessel.control.addNode(spaceCenter.ut, OrbitLifter.getLiftUpDV(this, satellite.orbit.apoapsis).toFloat(), 0f, 0f)
        val endUt = node.ut + period
        val searchingPeriapsis = satellite.sphereOfInfluence / 10

        var time: Double? = null
        while (node.ut < endUt) {
            val nextOrbit = node.orbit.nextOrbit
            val currentDt = nextOrbit?.let { dt / 10 } ?: dt
            if (nextOrbit != null) {
                if (nextOrbit.body.name != satellite.name) {
                    Log.w("Error while calculating orbit")
                    return@with
                }
                val periapsis = nextOrbit.periapsisAltitude
                if (periapsis < searchingPeriapsis) {
                    time = node.ut
                    break
                }
            }
            node.ut += currentDt
        }

        if (time == null) {
            Log.w("Time to start not found")
            node.remove()
            return@with
        }

        NodeExecutor.execFirst(this)

        KspUtils.warpByRails(this, spaceCenter.ut + vessel.orbit.timeToSOIChange + 10)

        val circularizingNode = vessel.control.addNode(spaceCenter.ut + vessel.orbit.timeToPeriapsis, 0f, 0f, 0f)
        val orbitHeight = vessel.orbit.periapsis + 1

        var step = -10f
        while (Math.abs(step) > 0.001f) {
            circularizingNode.prograde += step
            val hasApoapsis = circularizingNode.orbit.timeToSOIChange.isNaN()
            if (!hasApoapsis) {
                continue
            }
            val apoapsis = circularizingNode.orbit.apoapsis
            if (step < 0 && apoapsis < orbitHeight || step > 0 && apoapsis > orbitHeight) {
                step /= -10f
            }
        }
        NodeExecutor.execFirst(this)

    }

}