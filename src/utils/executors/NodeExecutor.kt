package utils.executors

import krpc.client.services.SpaceCenter
import utils.KspProgram
import utils.KspUtils
import utils.Utils
import utils.log.Log


object NodeExecutor {

    fun execFirst(program: KspProgram, timeForRotating: Double = 60.0) = with(program) {
        val node = vessel.control.nodes.getOrNull(0)
        if (node == null) {
            Log.w("There are no nodes for vessel")
            return@with
        }

        val nodeReferenceFrame = node.referenceFrame
        val nodeDV = node.deltaV
        val force = vessel.availableThrust.toDouble()
        val isp = vessel.specificImpulse * vessel.orbit.body.surfaceGravity
        val massBegin = vessel.mass.toDouble()
        val massEnd = massBegin / Math.exp(nodeDV / isp)
        val flowRate = force / isp
        val burnTime = (massBegin - massEnd) / flowRate
        val burnTime2 = burnTime / 2

        Log.d("Executing node. dV: $nodeDV, burnTime: $burnTime")

        val timeStartRotating = node.ut - timeForRotating - burnTime2
        if (spaceCenter.ut > timeStartRotating) {
            Log.w("There is no time for rotating")
            return@with
        }
        Log.d("Waiting for rotating...")
        KspUtils.warpByRails(this, timeStartRotating)

        Log.d("Rotating")
        vessel.autoPilot.sasMode = SpaceCenter.SASMode.MANEUVER
        KspUtils.sleep {
            val referenceFrame = vessel.orbitalReferenceFrame
            val cos = Utils.cos(node.burnVector(referenceFrame), vessel.direction(referenceFrame))
            return@sleep cos < 0.999
        }

        val timeStartBurn = node.ut - burnTime2
        if (spaceCenter.ut > timeStartBurn) {
            Log.w("There is no time for burn")
            return@with
        }
        Log.d("Waiting for burn...")
        KspUtils.warpByRails(this, timeStartBurn)

        Log.d("Burn")
        var vdot = 1.0
        while (vdot > 0) {
            val remainingBurnVector = node.remainingBurnVector(nodeReferenceFrame)
            val vesselDirection = vessel.direction(nodeReferenceFrame)

            val changingThrottle = Utils.length(remainingBurnVector)
            if (changingThrottle > 0.1) {
                val maxThrust = vessel.maxThrust
                if (maxThrust <= 0) {
                    Log.w("Vessel max thrust <= 0")
                    return@with
                }
                val mass = vessel.mass
                if (mass <= 0) {
                    Log.w("Vessel mass <= 0")
                    return@with
                }
                val maxAcc = maxThrust / mass
                val throttle = Math.min(node.remainingDeltaV / maxAcc, 1.0).toFloat()
                vessel.control.throttle = throttle
            }

            checkStageAndDecuple()
            KspUtils.pause()
            vdot = Utils.vdot(remainingBurnVector, vesselDirection)
        }

        vessel.control.throttle = 0f

        node.remove()
        Log.d("Node execution done")
    }


}