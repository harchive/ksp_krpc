package utils.executors

import krpc.client.services.SpaceCenter
import utils.KspProgram
import utils.log.Log


object Fixer {

    fun fixY(program: KspProgram, height: Long) = with(program) {

        vessel.control.sas = false

        vessel.autoPilot.engage()
        vessel.autoPilot.targetPitchAndHeading(90f, 0f)
        vessel.control.throttle = 0f
        vessel.control.activateNextStage()

        while (true) {
            vessel.control.throttle = calcThrottle(vessel, height, false)
        }
    }

    private fun calcThrottle(vessel: SpaceCenter.Vessel, height: Long, log: Boolean): Float {
        val flight = vessel.flight(vessel.orbit.body.referenceFrame)

        val altitude = flight.surfaceAltitude
        if (altitude >= height) {
            if (log) {
                Log.d("Vessel altitude: $altitude >= targetHeight: $height => 0")
            }
            return 0f
        }

        val velocity = flight.verticalSpeed
        if (velocity < 0) {
            if (log) {
                Log.d("Velocity: $velocity > 0 => 1")
            }
            return 1f
        }


        val timeToVelocityIs0 = velocity / flight.gForce
        val maxAltitude = altitude + velocity * timeToVelocityIs0 - flight.gForce * timeToVelocityIs0 / 2

        if (maxAltitude > height) {
            if (log) {
                Log.d("Max altitude: $maxAltitude >= targetHeight: $height => 0")
            }
            return 0f
        }

        if (log) {
            Log.d("Max altitude: $maxAltitude < targetHeight: $height => 0")
        }
        return 1f
    }

}