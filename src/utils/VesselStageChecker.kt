package utils

import utils.log.Log


class VesselStageChecker(val kspProgram: KspProgram) {

    private var lastMaxTrust = -1f
    private var lastMaxTrustCheckingTime = kspProgram.spaceCenter.ut

    fun checkAndDecuple() = with(kspProgram) {
        val pause = if (lastMaxTrust < 0) 3f else 0.1f
        val now = kspProgram.spaceCenter.ut
        if (now - lastMaxTrustCheckingTime < pause) {
            return
        }
        lastMaxTrustCheckingTime = now

        val throttle = vessel.control.throttle
        if (throttle <= 0.001) {
            return
        }
        val maxTrust = vessel.availableThrust
        val lastMaxTrust = lastMaxTrust

        this@VesselStageChecker.lastMaxTrust = maxTrust

        if (lastMaxTrust < 0) {
            return
        }
        if (lastMaxTrust > maxTrust) {
            Log.d("[STAGE]")
            val lastThrottle = kspProgram.vessel.control.throttle
            vessel.control.throttle = 0f
            KspUtils.pause()
            vessel.control.activateNextStage()
            KspUtils.pause()
            vessel.control.throttle = lastThrottle
            this@VesselStageChecker.lastMaxTrust = -1f
        }
    }

}