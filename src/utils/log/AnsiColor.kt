package utils.log


enum class AnsiColor(val code: String) {

    RESET("\u001B[0m"),
    BLACK("\u001B[30m"),
    RED("\u001B[31m"),
    GREEN("\u001B[32m"),
    YELLOW("\u001B[33m"),
    BLUE("\u001B[34m"),
    PURPLE("\u001B[35m"),
    CYAN("\u001B[36m"),
    WHITE("\u001B[37m");

    companion object {

        fun createString(color: AnsiColor, text: String) =
                createStringWithCodes(arrayListOf(Pair(color, text)))

        fun createStringWithCodes(items: List<Pair<AnsiColor, String>>): String {
            val builder = StringBuilder()
            items.forEach {
                builder.append(it.first.code)
                builder.append(it.second)
            }
            builder.append(RESET.code)
            return builder.toString()
        }

    }

    fun createString(text: String) = createString(this, text)


}